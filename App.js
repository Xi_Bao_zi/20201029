import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';

export default class App extends React.Component {
 
  constructor (props) {
    super(props);

    this.state = {
       userAcc :""
    }
  }

  buttonClick = (res) => {
    this.setState({userAcc: res});
    console.log(this.state.userAcc)
  }

  consoleText = () => {
  console.log("OK");
  let acc = this.state.userAcc;
  this.buttonClick(acc);
}


 render(){
  return (
    <View style={styles.container}>
      <Text>Open up App.js to start working on your app!</Text>
      <StatusBar style="auto" />

      <TextInput
        style={{ height: 40, borderColor: 'gray', borderWidth: 1}}
        ref = {(input) => {this.userAcc = input}}
        onChangeText={(userAcc) => this.setState({userAcc})}
        value={this.state.userAcc}
    />
    <Button title="Press me" onPress={this.consoleText} />

    </View>
  );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

